from os.path import dirname, join

enmap = join(dirname(__file__), 'enmap_berlin.bsq')
hires = join(dirname(__file__), 'hires_berlin.bsq')
landcover_polygons = join(dirname(__file__), 'landcover_berlin_polygon.gpkg')
landcover_points = join(dirname(__file__), 'landcover_berlin_point.gpkg')
library = join(dirname(__file__), 'library_berlin.sli')
enmap_srf_library = join(dirname(__file__), 'enmap_srf_library.gpkg')
